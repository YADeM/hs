<?php
include_once 'conf.php';
include_once '../header.php';

$r='';
$r.='<body>';
$auth = new auth();
//~ authorization
if (isset($_POST['send'])) {
	if (!$auth->authorization()) {
		$error = $auth->error_reporting();
	}
}

//~ user exit
if (isset($_GET['exit'])) $auth->exit_user();

//~ Check auth
if ($auth->check()) {
        $r.='<script language="JavaScript" type="text/javascript"><!--
			location="main.php"
			//-->
			</script>
			';
} else {
	$r.= '
	
    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">
            <div class="inner cover">
	
	<form action="" method="post" class="form-signin" role="form">
            <p class="lead"><a href="join.php" class="btn btn btn-success btn-block">Регистрация</a></p>
		<label for="logininput" class="sr-only">Логин</label>
                <input type="text" placeholder="Логин" id="logininput" class="form-control" name="login" value="'.@$_POST['login'].'" required/>
                <label for="passinput" class="sr-only">Пароль</label>
                <input type="password" placeholder="Пароль" id="passinput" class="form-control" name="passwd" id="" required/>
                <input type="checkbox" name="remember" value="remeber-me"> Запомнить меня
                <input type="submit" value="Вход" name="send" class="btn btn-primary btn-block" />
	</form>
	';
	if (isset($error)) $r.= '<div class="recovery-container">'.$error.'. <a href="recovery.php">Восстановить пароль</a></div><br/>';
	$r.='</div>
		</div>
            </div>
        </div>';
}

print $r;

include_once '../footer.php';?>