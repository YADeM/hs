<?php
include_once 'conf.php';

$mysqli = new mysqli($db_host, $db_login, $db_passwd, $db_name);

$answ = $_POST['answ'];
$quest = $_POST['quest'];
$ra = $_POST['isrght'];

if (mysqli_connect_errno()) { 
   printf("Подключение к серверу MySQL невозможно. Код ошибки: %s\n", mysqli_connect_error()); 
   exit;   
};

if(isset($quest)){
    $stmt = $mysqli->prepare("INSERT INTO qst (`question`) VALUES (?)");
        $stmt->bind_param('s', $quest);
        $stmt->execute();
        
        if ($stmt->errno) {
            die('Select Error (' . $stmt->errno . ') ' . $stmt->error);
            }
        $stmt->close();
}
$quest_id=$mysqli->insert_id;

$capture_field_vals ="";
if(isset($answ)){
    foreach($answ as $key => $text_field){
        printf ("$text_field,$quest_id, $ra");
        echo('<br>');
        $stmt = $mysqli->prepare("INSERT INTO answ ( `answer`, `qst-num`, `isright` ) VALUES ( ?,?,?)");
        $stmt->bind_param('sss', $text_field, $quest_id, $ra);
        $stmt->execute();
        
        if ($stmt->errno) {
            die('Select Error (' . $stmt->errno . ') ' . $stmt->error);
            }
        $stmt->close();
    }
}

$mysqli->close();

?>