<?php
include_once 'conf.php';
include_once '../header.php';
$auth = new auth();
$form = '
<body>
    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">
            <div class="inner cover">
               
                        <form action="" method="post" class="form-signin" role="form">
                        <!--<a href="index.php"><input type="button" value="Вход" class="btn btn-default btn-block"/></a><br>-->
                        <p class="lead"><a href="index.php" class="btn btn btn-success btn-block">Вход</a></p>

                        <label for="logininput" class="sr-only">Логин</label>
                        <input type="text" placeholder="Логин" id="logininput" class="form-control" name="login" value="' . @$_POST['login'] . '" required/>
                        <label for="passinput" class="sr-only">Пароль</label>
                        <input type="password" placeholder="Пароль" id="passinput" class="form-control" name="passwd" value="" required/>
                        <label for="retpassinput" class="sr-only">Повторите пароль</label>
                        <input type="password" placeholder="Повторите пароль" id="retpassinput" class="form-control" name="passwd2" value="" required/>
                        <label for="emailinput" class="sr-only">E-mail</label>
                        <input type="email" placeholder="E-mail" id="emailinput" class="form-control" name="mail" value="' . @$_POST['mail'] . '" required/>
                        <input type="submit" value="Зарегистрироваться" name="send" class="btn btn-primary btn-block"/>
            </form>
                    </div>
		</div>
            </div>
        </div>
	';
if (isset($_POST['send'])) {
    if ($auth->reg()) {
        print '
			Registration successful. <a href="index.php">Login</a>.
		';
    } else {
        print $auth->error_reporting();
        print $form;
    }
} else
    print $form;
?>
<?php include_once '../footer.php' ?>