<?php
include_once 'conf.php';
include_once '../header.php';
$username = $_SESSION['login_user'];
$auth = new auth();
if ($auth->check()) {
$userid = $_SESSION['id_user'];
} else {
	include_once 'index.php';
	exit;
}
?>

<body>
<script>
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var counter         = 1;
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input class="form-control" placeholder="Enter answer" type="text" name="answ[]"><input type="radio" name="isrght" value="' + (counter + 1) + '" required></div>'); //add input box
            counter++;
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
 });

$('#ask input').on('change mousedown', selector, function() {
   alert($('input[name=isrght]:checked', '#ask').val()); 
});
</script>
<div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">
            <div class="inner cover">
                <form action="addq.php" method="post" class="form-signin form-inline" id="ask" role="form" name="myForm">
                    <div class="input_fields_wrap">
                        <input class="form-control" placeholder="Enter question" type="text" name="quest">
                        <div class="form-group">
                            <button class="add_field_button btn btn-default">Add more answers</button>
                            <input class="form-control" placeholder="Enter answer" type="text" name="answ[]"><input type="radio" name="isrght" value="1" required>
                        </div>
                    </div>
                    <input type="submit" value="Send" name="send" class="btn btn-primary btn-block"/>
                </form>

</div>
</div>
</div>
</div>
<?php include_once '../footer.php'?>