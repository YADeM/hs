﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.3.358.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 26.06.2015 22:55:02
-- Версия сервера: 5.5.25
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

-- 
-- Установка базы данных по умолчанию
--
USE hs;

--
-- Описание для таблицы answ
--
DROP TABLE IF EXISTS answ;
CREATE TABLE answ (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  answer VARCHAR(255) NOT NULL COMMENT 'answer for the question',
  `qst-num` VARCHAR(255) NOT NULL COMMENT 'question number',
  isright TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0 not correct answer, 1 correct',
  PRIMARY KEY (id),
  UNIQUE INDEX `anws-id` (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 100
AVG_ROW_LENGTH = 165
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'answer table';

--
-- Описание для таблицы qst
--
DROP TABLE IF EXISTS qst;
CREATE TABLE qst (
  id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  question VARCHAR(255) DEFAULT NULL COMMENT 'question',
  visible TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'question will be visible? non visible by default',
  PRIMARY KEY (id),
  UNIQUE INDEX `qest-id` (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 48
AVG_ROW_LENGTH = 348
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'question table';

--
-- Описание для таблицы session
--
DROP TABLE IF EXISTS session;
CREATE TABLE session (
  id_sess INT(5) NOT NULL AUTO_INCREMENT,
  id_user INT(5) NOT NULL,
  code_sess VARCHAR(15) NOT NULL,
  user_agent_sess VARCHAR(255) NOT NULL,
  date_sess TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  used_sess INT(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (id_sess)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы users
--
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id_user INT(5) NOT NULL AUTO_INCREMENT,
  login_user VARCHAR(60) NOT NULL,
  passwd_user VARCHAR(255) NOT NULL,
  mail_user VARCHAR(255) NOT NULL,
  admin INT(1) NOT NULL,
  key_user VARCHAR(10) NOT NULL,
  PRIMARY KEY (id_user)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Вывод данных для таблицы answ
--
INSERT INTO answ VALUES
(1, 'test', '1', 0),
(2, '1231', '1', 0),
(3, '1', '2', 0),
(4, '1', '0', 0),
(5, '1', '0', 0),
(6, '1', '3', 0),
(7, '12', '4', 0),
(8, '234', '5', 0),
(9, '1', '6', 0),
(10, '1', '7', 0),
(11, '123', '8', 0),
(12, '123', '9', 0),
(13, '123', '9', 0),
(14, '123', '9', 0),
(15, '12da', '9', 0),
(16, 'asda', '9', 0),
(17, '', '10', 0),
(18, '', '11', 0),
(19, '', '12', 0),
(20, '', '13', 0),
(21, '', '14', 0),
(22, '', '15', 0),
(23, '', '16', 0),
(24, '', '17', 0),
(25, '', '18', 0),
(26, '', '19', 0),
(27, '', '20', 0),
(28, '', '21', 0),
(29, '213', '24', 0),
(30, '123', '24', 0),
(31, '123', '24', 0),
(32, '213', '25', 0),
(33, '123', '25', 0),
(34, '123', '25', 0),
(35, '123', '26', 0),
(36, '', '26', 0),
(37, '', '26', 0),
(38, '', '26', 0),
(39, 'qw', '32', 0),
(40, 'qwe', '32', 1),
(41, '1', '33', 0),
(42, '2', '33', 2),
(43, '3', '33', 0),
(44, '4', '33', 0),
(45, '5', '33', 0),
(46, '1', '34', 0),
(47, '2', '34', 2),
(48, '3', '34', 0),
(49, '4', '34', 0),
(50, '5', '34', 0),
(51, '1', '35', 0),
(52, '2', '35', 0),
(53, '3', '35', 0),
(54, '4', '35', 0),
(55, '5', '35', 0),
(56, '1', '36', 0),
(57, '2', '36', 0),
(58, '3', '36', 0),
(59, '4', '36', 0),
(60, '5', '36', 0),
(61, 'non', '37', 0),
(62, 'none', '37', 0),
(63, 'check', '37', 0),
(64, '1', '38', 0),
(65, '2', '38', 0),
(66, '3', '38', 0),
(67, '4', '38', 0),
(68, '5', '38', 0),
(69, '1', '39', 0),
(70, '2', '39', 4),
(71, '3', '39', 0),
(72, '4', '39', 0),
(73, '1', '40', 4),
(74, '2', '40', 4),
(75, '3', '40', 4),
(76, '4', '40', 4),
(77, '1', '41', 5),
(78, '2', '41', 5),
(79, '3', '41', 5),
(80, '4', '41', 5),
(81, '12', '42', 2),
(82, '123', '42', 2),
(83, '213', '42', 2),
(84, '123', '43', 3),
(85, '123', '43', 3),
(86, '123', '43', 3),
(87, '123', '44', 3),
(88, '123', '44', 3),
(89, '123', '44', 3),
(90, '123', '44', 3),
(91, '12', '45', 1),
(92, 'qwe', '45', 1),
(93, 'qwe', '45', 1),
(94, 'qwe', '45', 1),
(95, '', '46', 1),
(96, 'Джордж Вашингтон', '47', 1),
(97, 'Томас Джефферсон', '47', 1),
(98, 'Джеймс Мэдисон', '47', 1),
(99, 'Джон Адамс', '47', 1);

-- 
-- Вывод данных для таблицы qst
--
INSERT INTO qst VALUES
(1, NULL, 0),
(2, '1', 0),
(3, 'test', 0),
(4, 'tt', 0),
(5, 'tt6', 0),
(6, 'tter', 0),
(7, 'Array', 0),
(8, 'werwer', 0),
(9, 'tset', 0),
(10, '', 0),
(11, '', 0),
(12, '', 0),
(13, '', 0),
(14, '', 0),
(15, '', 0),
(16, '', 0),
(17, '', 0),
(18, '', 0),
(19, '', 0),
(20, '', 0),
(21, '', 0),
(22, 'wer', 0),
(23, '1231weq', 0),
(24, '', 0),
(25, '', 0),
(26, '12', 0),
(27, '13', 0),
(28, '13', 0),
(29, '12', 0),
(30, '234', 0),
(31, '345', 0),
(32, 'qwdas', 0),
(33, '123123', 0),
(34, '123', 0),
(35, '12312', 0),
(36, 'quest', 0),
(37, 'quest', 0),
(38, 'quest', 0),
(39, '123123', 0),
(40, '1231231', 0),
(41, '324', 0),
(42, '123', 0),
(43, '12312', 0),
(44, '123', 0),
(45, '123123', 0),
(46, '12312', 0),
(47, 'Первый президент США', 0);

-- 
-- Вывод данных для таблицы session
--

-- Таблица hs.session не содержит данных

-- 
-- Вывод данных для таблицы users
--
INSERT INTO users VALUES
(1, 'YADeM', '698299f72906301934cad8f614c1fbdf', 'yadem@list.ru', 0, '07JQxZdesn');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;