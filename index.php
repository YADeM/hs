<?php include_once 'admin/db_conf.php';
session_start();
include_once 'header.php';
$ot='';
$ott='';
$mysqli = new mysqli($db_host, $db_login, $db_passwd, $db_name);

// mikrotik post variables set start
   if(isset($_POST['mac'])) {
       $mac=$_POST['mac'];
       $_SESSION['mac']=$mac;
   }

    if(isset($_POST['password'])) {
       $password=$_POST['password'];
       $_SESSION['password']=$password;
   }
   
   if(isset($_POST['ip'])) {
       $ip=$_POST['ip'];
       $_SESSION['ip']=$ip;
   }
   
   if(isset($_POST['username'])) {
       $username=$_POST['username'];
       $_SESSION['username']=$username;
   }
   
   if(isset($_POST['link-login'])) {
       $linklogin=$_POST['link-login'];
       $_SESSION['linklogin']=$linklogin;
   }
   
   if(isset($_POST['link-orig'])) {
       $linkorig=$_POST['link-orig'];
       $_SESSION['linkorig']=$linkorig;
   }
   
   if(isset($_POST['error'])) {
       $error=$_POST['error'];
       $_SESSION['erroe']=$error;
   }
   
   if(isset($_POST['chap-id'])) {
       $chapid=$_POST['chap-id'];
       $_SESSION['chapid']=$chapid;
   }
   
   if(isset($_POST['chap-challenge'])) {
       $chapchallenge=$_POST['chap-challenge'];
       $_SESSION['chapchallenge']=$chapchallenge;
   }
   
   if(isset($_POST['link-login-only'])) {
       $linkloginonly=$_POST['link-login-only'];
       $_SESSION['linkloginonly']=$linkloginonly;
	}
   
   if(isset($_POST['link-orig-esc'])) {
        $linkorigesc=$_POST['link-orig-esc'];
        $_SESSION['linkorigesc']=$linkorigesc;
   }
   
   if(isset($_POST['mac-esc'])) {
        $macesc=$_POST['mac-esc'];
        $_SESSION['macesc']=$macesc;
   }
// mikrotik post variables set end

   
if (mysqli_connect_errno()) { 
   printf("Подключение к серверу MySQL невозможно. Код ошибки: %s\n", mysqli_connect_error()); 
   exit;   
}

if ($result = $mysqli->query('SELECT qst.id, qst.question FROM qst ORDER BY qst.id DESC LIMIT 1;')) { 
    while( $row = $result->fetch_assoc()){
                $id=$row['id'];
                $id = $mysqli->real_escape_string($id);
                $q1=$row['question'];
                $q1 = $mysqli->real_escape_string($q1);
        }
    }
    
if ($result = $mysqli->query('SELECT answ.answer FROM answ INNER JOIN qst ON answ.`qst-num` = qst.id WHERE qst.id = '.$id.' ORDER BY qst.id ASC;')) {
    $rowcnt = $result->num_rows;
    $row_num=1;
    $ot .='<div class="row answers">
                           ';
    while( $row = $result->fetch_assoc()){
                if ($rowcnt > 2){
                    if ($row_num % 2 == 1) {
                        $ot .='<div class="col-sm-6"><input type="submit" value="'.$row['answer'].'" name="answer" class="btn btn-default btn-block btn-answ"></div>
                           ';
                    } else {
                        $ot .='<div class="col-sm-6"><input type="submit" value="'.$row['answer'].'" name="answer" class="btn btn-default btn-block btn-answ"></div>
                            </div>
                            <div class="row answers">
                            ';
                    }
                    $row_num ++;
                } else {
                    $ot .='<div class="col-sm-6"><input type="submit" value="'.$row['answer'].'" name="answer" class="btn btn-default btn-block btn-answ"></div>
                           ';
                }
		
        }
        $ot.= '</div>
            ';
    }

   if(isset($_POST['answer'])) {
       $ans = $_POST['answer'];
       $isrghtchk = 1;
       $id = $mysqli->real_escape_string($id);
       if ($result = $mysqli->query('SELECT answ.answer, answ.isright FROM answ INNER JOIN qst ON answ.`qst-num` = qst.id WHERE qst.id = '.$id.' ORDER BY qst.id ASC;')) { 
            while( $row = $result->fetch_assoc()){
                if ($row['isright'] == $isrghtchk){
                    $answ=$row['answer'];
					$url= $_SESSION['linkloginonly'].'?username='.$mikuname.'&password='.$mikpass.'&dst='.$_SESSION['linkorig'];
					$urlout = $_SESSION['linkloginonly'] .'?logout';
					session_destroy();
                    break;
                }
                $isrghtchk ++;
        }
    }
            if ($ans == $answ){ //check if form was submitted
			$ot .= '<input type="submit" value="Правильный ответ!"/>';
			$ott.= '<form name="redirect" action="'.$url.'" method="post">
					<input type="submit" value="continue">
					</form>
					<script language="JavaScript">
						<!--
							document.redirect.submit();
						//-->
					</script>';
                //session_destroy();
                } else {
                $ot.= '<span class="label label-danger">Неправильный ответ!</span>';
                }
   }
    
$mysqli->close();
?>
    <body>
    <div class="site-wrapper">

      <div class="site-wrapper-inner">
        <div class="cover-container">

            <div class="inner cover">
                <form action="" method="post" class="form-ans" role="form">
                <div class="row head">
                    <div class="col-xs-12">To get access, answer question</div>
                </div>
                <div class="row question">
                    <div class="col-xs-12"><h1 class="cover-heading"><?php echo ($q1);?></h1></div>
                </div>
				
                <div class="row bottom">
                    <div class="col-xs-12"><?php echo($ot);?></div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
echo $ott;
?>
<?php include_once 'footer.php'?>